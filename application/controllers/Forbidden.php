<?php

class Forbidden extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $view_data["heading"] = "403 ممنوع";
        $view_data["message"] = "ليس لديك إذن للدخول إلى هذه الصفحة.";
        if ($this->input->is_ajax_request()) {
            $view_data["no_css"] = true;
        }
        $this->load->view("errors/html/error_general", $view_data);
    }

}
