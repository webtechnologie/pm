<?php

/* NOTE: DO NOT CHANGE THIS FILE. IF YOU WANT TO UPDATE THE LANGUAGE THEN COPY THIS FILE TO custom_lang.php AND UPDATE THERE */

/* language locale */
$lang["language_locale"] = "ar"; //locale code
$lang["language_locale_long"] = "ar-SA"; //long locale code

/* common */
$lang["add"] = "إضافة";
$lang["edit"] = "تعديل";
$lang["close"] = "إغلاق";
$lang["cancel"] = "إلغاء";
$lang["save"] = "حفظ";
$lang["delete"] = "حذف";
$lang["description"] = "وصف";
$lang["admin"] = "المشرف";
$lang["manager"] = "مدير";
$lang["options"] = "خيارات";
$lang["id"] = "الرقم ";
$lang["name"] = "الإسم";
$lang["email"] = "البريد الإلكتروني";
$lang["username"] = "اسم المستخدم";
$lang["password"] = "كلمة السر";
$lang["retype_password"] = "أعد إدخال كلمة السر";
$lang["previous"] = "السابق";
$lang["next"] = "التالي";
$lang["active"] = "نشط";
$lang["inactive"] = "غير نشط";
$lang["status"] = "الحالة";
$lang["start_date"] = "تاريخ البدء";
$lang["end_date"] = "تاريخ الانتهاء";
$lang["start_time"] = "وقت البدء";
$lang["end_time"] = "وقت النهاية";
$lang["deadline"] = "الموعد النهائي";
$lang["added"] = "تمت إضافته";
$lang["created_date"] = "تاريخ الإنشاء";
$lang["created"] = "تم إنشائه";
$lang["created_by"] = "تم الإنشاء بواسطة";
$lang["updated"] = "تم تحديثه";
$lang["deleted"] = "تم حذفه";
$lang["currency"] = "عملة";
$lang["new"] = "الجديد";
$lang["open"] = "مفتوح";
$lang["closed"] = "تمت";
$lang["date"] = "التاريخ";
$lang["yes"] = "نعم";
$lang["no"] = "لا";
$lang["add_more"] = "أضف المزيد";
$lang["crop"] = "إقتصاص";
$lang["income"] = "الإيرادات";
$lang["income_vs_expenses"] = "الإيرادات مقابل المصروفات";

$lang["title"] = "عنوان";
$lang["reset"] = "إعادة";
$lang["share_with"] = "شارك مع";
$lang["company_name"] = "اسم الشركة";
$lang["address"] = "عنوان";
$lang["city"] = "المدينة";
$lang["state"] = "المحافظة";
$lang["zip"] = "الرمز البريدي";
$lang["country"] = "البلد";
$lang["phone"] = "الهاتف";
$lang["private"] = "خاص";
$lang["website"] = "الموقع الكتروني";

$lang["sunday"] = "الأحد";
$lang["monday"] = "الإثنين";
$lang["tuesday"] = "الثلاثاء";
$lang["wednesday"] = "الأربعاء";
$lang["thursday"] = "الخميس";
$lang["friday"] = "الجمعة";
$lang["saturday"] = "السبت";

$lang["daily"] = "يوميا";
$lang["monthly"] = "شهريا";
$lang["weekly"] = "أسبوعيا";
$lang["yearly"] = "سنويا";

$lang["see_all"] = "اظهار الكل";

/* messages */
$lang["error_occurred"] = "عذرا، حدث خطأ أثناء معالجة الإجراء! <br /> الرجاء إعادة المحاولة لاحقا.";
$lang["field_required"] = "هذا الحقل إجباري.";
$lang["end_date_must_be_equal_or_greater_than_start_date"] = "يجب أن يكون تاريخ الانتهاء مساويا أو أكبر من تاريخ البدء.";
$lang["date_must_be_equal_or_greater_than_today"] = "يجب أن يكون التاريخ مساويا أو أكبر من اليوم.";
$lang["enter_valid_email"] = "رجاء قم بإدخال بريد الكتروني صحيح.";
$lang["enter_same_value"] = "الرجاء إدخال نفس المحتوى مرة أخرى.";
$lang["record_saved"] = "تم حفظ السجل.";
$lang["record_updated"] = "تم تحديث السجل.";
$lang["record_cannot_be_deleted"] = "السجل قيد الاستخدام، لا يمكنك حذف السجل!";
$lang["record_deleted"] = "تم حذف السجل.";
$lang["record_undone"] = "تم التراجع عن السجل.";
$lang["settings_updated"] = "تم تحديث الإعدادات.";
$lang["enter_minimum_6_characters"] = "الرجاء إدخال 6 أحرف على الأقل.";
$lang["message_sent"] = "تم إرسال الرسالة.";
$lang["invalid_file_type"] = "نوع الملف غير مسموح به.";
$lang["something_went_wrong"] = "شيء ما حدث بشكل خاطئ!";
$lang["duplicate_email"] = "عنوان البريد الإلكتروني الذي أدخلته مسجل بالفعل.";
$lang["comment_submited"] = "تم إرسال التعليق";
$lang["no_new_messages"] = "ليست لديك أية رسائل جديدة";
$lang["sent_you_a_message"] = "أرسلت لك رسالة";
$lang["max_file_size_3mb_message"] = "يجب ألا يزيد حجم الملف عن 3 ميغابايت";
$lang["keep_it_blank_to_use_default"] = "ضعه فارغا لاستخدام الإعداد الافتراضي";
$lang["admin_user_has_all_power"] = "المستخدم المسؤول لديه القدرة على الوصول / تعديل كل شيء في هذا النظام!";
$lang["no_posts_to_show"] = "لا توجد مشاركات لعرضها";

/* team_member */
$lang["add_team_member"] = "إضافة عضو";
$lang["edit_team_member"] = "تعديل عضو الفريق";
$lang["delete_team_member"] = "حذف عضو الفريق";
$lang["team_member"] = "عضو الفريق";
$lang["team_members"] = "أعضاء الفريق";
$lang["active_members"] = "لأعضاء النشطون";
$lang["inactive_members"] = "أعضاء غير نشطين";
$lang["first_name"] = "الاسم الاول";
$lang["last_name"] = "اسم العائلة";
$lang["mailing_address"] = "البريد الالكتروني";
$lang["alternative_address"] = "المدينة";
$lang["phone"] = "الهاتف";
$lang["alternative_phone"] = "الجوال";
$lang["gender"] = "الجنس";
$lang["male"] = "ذكر";
$lang["female"] = "أنثى";
$lang["date_of_birth"] = "تاريخ الميلاد";
$lang["date_of_hire"] = "تاريخ التوظيف";
$lang["ssn"] = "رقم بطاقة الهوية";
$lang["salary"] = "راتب";
$lang["salary_term"] = "مدة الراتب";
$lang["job_info"] = "معلومات الوظيفة";
$lang["job_title"] = "المسمى الوظيفي";
$lang["general_info"] = "معلومات عامة";
$lang["account_settings"] = "إعدادت الحساب";
$lang["list_view"] = "عرض القائمة";
$lang["profile_image_changed"] = "تم تغيير صورة الملف الشخصي.";
$lang["send_invitation"] = "إرسال دعوة";
$lang["invitation_sent"] = "تم إرسال الدعوة.";
$lang["reset_info_send"] = "تم إرسال الرسالة الإلكترونية! <br /> يرجى التحقق من بريدك الإلكتروني للحصول على الإرشادات.";
$lang["profile"] = "الملف الشخصي";
$lang["my_profile"] = "حسابي";
$lang["change_password"] = "تغيير كلمة المرور";
$lang["social_links"] = "روابط اجتماعية";
$lang["view_details"] = "عرض التفاصيل";
$lang["invite_someone_to_join_as_a_team_member"] = "دعوة شخص للانضمام كعضو في الفريق.";

/* team */
$lang["add_team"] = "إضافة فريق";
$lang["edit_team"] = "تعديل الفريق";
$lang["delete_teamn"] = "حذف الفريق";
$lang["team"] = "الفريق";
$lang["select_a_team"] = "اختيار فريق";

/* dashboard */
$lang["dashboard"] = "الرئيسية";

/* attendance */
$lang["add_attendance"] = "إضافة الوقت يدويا";
$lang["edit_attendance"] = "تعديل بطاقة الوقت";
$lang["delete_attendance"] = "حذف بطاقة الوقت";
$lang["attendance"] = "الحضور";
$lang["clock_in"] = "ساعة الدخول";
$lang["clock_out"] = "ساعة الخروج";
$lang["in_date"] = "في تاريخ";
$lang["out_date"] = "تاريخ الخروج";
$lang["in_time"] = "في الوقت";
$lang["out_time"] = "وقت الخروج";
$lang["clock_started_at"] = "بدأت الساعة في";
$lang["you_are_currently_clocked_out"] = "أنت حاليا غير مسجل";
$lang["members_clocked_in"] = "الأعضاء المسجلين";
$lang["members_clocked_out"] = "الأعضاء غيرالمسجلين";
$lang["my_time_cards"] = "بطاقات وقتي";
$lang["timecard_statistics"] = "إحصاءات بطاقة الوقت";
$lang["total_hours_worked"] = "مجموع ساعات العمل";
$lang["total_project_hours"] = "مجموع ساعات المشروع";

/* leave types */
$lang["add_leave_type"] = "إضافة نوع الإجازة";
$lang["edit_leave_type"] = "تعديل نوع الإجازة";
$lang["delete_leave_type"] = "حذف نوع الإجازة";
$lang["leave_type"] = "نوع الإجازة";
$lang["leave_types"] = "أنواع الإجازة";

/* leave */
$lang["apply_leave"] = "تطبيق الإجازة";
$lang["assign_leave"] = "تعيين الإجازة";
$lang["leaves"] = "إجازة";
$lang["pending_approval"] = "في انتظار الموافقات";
$lang["all_applications"] = "جميع التطبيقات";
$lang["duration"] = "المدة الزمنية";
$lang["single_day"] = "يوم واحد";
$lang["mulitple_days"] = "عدة أيام";
$lang["reason"] = "السبب";
$lang["applicant"] = "طالب وظيفة";
$lang["approved"] = "موافق عليه";
$lang["approve"] = "اعتماد";
$lang["rejected"] = "مرفوض";
$lang["reject"] = "رفض";
$lang["canceled"] = "ملغي ";
$lang["completed"] = "منجز";
$lang["pending"] = "قيد الانتظار";
$lang["day"] = "يوم";
$lang["days"] = "أيام";
$lang["hour"] = "ساعة";
$lang["hours"] = "ساعات";
$lang["application_details"] = "تفاصيل التطبيق";
$lang["rejected_by"] = "مرفوض من طرف";
$lang["approved_by"] = "تمت الموافقة عليه من قبل";
$lang["start_date_to_end_date_format"] = "%s إلى %s";
$lang["my_leave"] = "إجازتي";

/* events */
$lang["add_event"] = "إضافة حدث";
$lang["edit_event"] = "تعديل الحدث";
$lang["delete_event"] = "حذف الحدث";
$lang["events"] = "التقويم";
$lang["event_calendar"] = "تقويم الأحداث";
$lang["location"] = "الموقع";
$lang["event_details"] = "تفاصيل الحدث";
$lang["event_deleted"] = "تم حذف الحدث.";
$lang["view_on_calendar"] = "عرض على التقويم";
$lang["no_event_found"] = "لم يتم العثور على أي حدث!";
$lang["events_today"] = "أحداث اليوم";

/* announcement */
$lang["add_announcement"] = "إضافة إعلان";
$lang["edit_announcement"] = "تعديل الإعلان";
$lang["delete_announcement"] = "حذف الإعلان";
$lang["announcement"] = "إعلان";
$lang["announcements"] = "الإعلانات";
$lang["all_team_members"] = "جميع أعضاء الفريق";
$lang["all_team_clients"] = "جميع العملاء";

/* settings */
$lang["app_settings"] = "إعدادات التطبيق";
$lang["app_title"] = "عنوان التطبيق";
$lang["site_logo"] = "شعار الموقع";
$lang["invoice_logo"] = "شعار الفاتورة";
$lang["timezone"] = "المنطقة الزمنية";
$lang["date_format"] = "صيغة التاريخ";
$lang["time_format"] = "تنسيق الوقت";
$lang["first_day_of_week"] = "اليوم الأول من الأسبوع";
$lang["currency_symbol"] = "رمز العملة";
$lang["general"] = "عام";
$lang["general_settings"] = "الاعدادات العامة";
$lang["item_purchase_code"] = "البند رمز الشراء";
$lang["company"] = "الشركة";
$lang["company_settings"] = "إعدادات الشركة";
$lang["email_settings"] = "إعدادات البريد الإلكتروني";
$lang["payment_methods"] = "طرق الدفع";
$lang["email_sent_from_address"] = "البريد الإلكتروني المرسل من العنوان";
$lang["email_sent_from_name"] = "البريد الإلكتروني المرسل من الاسم";
$lang["email_use_smtp"] = "استخدام SMTP";
$lang["email_smtp_host"] = "مضيف SMTPt";
$lang["email_smtp_user"] = "مستخدم SMTP";
$lang["email_smtp_password"] = "كلمة مرور SMTP";
$lang["email_smtp_port"] = "منفذ SMTP";
$lang["send_test_mail_to"] = "أرسل رسالة اختبار إلى";
$lang["test_mail_sent"] = "تم إرسال بريد الاختبار!";
$lang["test_mail_send_failed"] = "فشل في إرسال البريد الإلكتروني الاختبار.";
$lang["settings"] = "إعدادات";
$lang["updates"] = "التحديثات";
$lang["current_version"] = "النسخة الحالية";
$lang["language"] = "لغة";
$lang["ip_restriction"] = "تقييد IP";
$lang["varification_failed_message"] = "عذرا، لم نتمكن من التحقق من رمز الشراء.";
$lang["enter_one_ip_per_line"] = "أدخل IP واحد في كل سطر. أبقيه فارغا . * لن يتأثر المستخدم المشرف.";
$lang["allow_timecard_access_from_these_ips_only"] = "السماح بالوصول إلى بطاقة الوقت من عناوين IP  الموجودة فقط.";
$lang["decimal_separator"] = "الفاصل العشري";
$lang["client_settings"] = "إعدادات العميل";
$lang["disable_client_login_and_signup"] = "تعطيل تسجيل دخول العميل والاشتراك";
$lang["disable_client_login_help_message"] = "لن تتمكن جهات اتصال العملاء من تسجيل الدخول / الاشتراك في هذا النظام حتى تعيد هذا الإعداد.";
$lang["who_can_send_or_receive_message_to_or_from_clients"] = "من يمكنه إرسال / استقبال رسالة إلى / من العملاء";

/* account */
$lang["authentication_failed"] = "تسجيل دخول خاطئ";
$lang["signin"] = "تسجيل الدخول";
$lang["sign_out"] = "تسجيل الخروج";
$lang["you_dont_have_an_account"] = "ليس لديك حساب؟";
$lang["already_have_an_account"] = "هل لديك حساب؟";
$lang["forgot_password"] = "هل نسيت كلمة المرور؟";
$lang["signup"] = "سجل";
$lang["input_email_to_reset_password"] = "أدخل بريدك الإلكتروني لإعادة تعيين كلمة المرور";
$lang["no_acount_found_with_this_email"] = "عذرا، لم يتم العثور على حساب باستخدام هذه الرسالة الإلكترونية.";
$lang["reset_password"] = "إعادة تعيين كلمة المرور";
$lang["password_reset_successfully"] = "تم إعادة تعيين كلمة المرور الخاصة بك بنجاح.";
$lang["account_created"] = "تم إنشاء حسابك بنجاح!";
$lang["invitation_expaired_message"] = "انتهت صلاحية الدعوة أو حدث خطأ ما";
$lang["account_already_exists_for_your_mail"] = "الحساب موجود بالفعل لعنوان بريدك الإلكتروني.";
$lang["create_an_account_as_a_new_client"] = "إنشاء حساب كعميل جديد.";
$lang["create_an_account_as_a_team_member"] = "إنشاء حساب كعضو في الفريق.";
$lang["create_an_account_as_a_client_contact"] = "إنشاء حساب كجهة اتصال عميل.";

/* messages */
$lang["messages"] = "الرسائل";
$lang["message"] = "رسالة";
$lang["compose"] = "إنشاء";
$lang["send_message"] = "إرسال رسالة";
$lang["write_a_message"] = "اكتب رسالة...";
$lang["reply_to_sender"] = "رد على المرسل...";
$lang["subject"] = "الموضوع";
$lang["send"] = "إرسال";
$lang["to"] = "إلى";
$lang["from"] = "من";
$lang["inbox"] = "صندوق الوارد";
$lang["sent_items"] = "البنود المرسلة";
$lang["me"] = "أنا";
$lang["select_a_message"] = "حدد رسالة لعرضها";

/* clients */
$lang["add_client"] = "إضافة عميل";
$lang["edit_client"] = "تعديل العميل";
$lang["delete_client"] = "حذف العميل";
$lang["client"] = "عميل";
$lang["clients"] = "العملاء";
$lang["client_details"] = "تفاصيل العميل";
$lang["due"] = "دين";

$lang["add_contact"] = "إضافةاتصال";
$lang["edit_contact"] = "تعديل لاتصال";
$lang["delete_contact"] = "حذف اتصال";
$lang["contact"] = "اتصل";
$lang["contacts"] = "جهات الاتصال";
$lang["users"] = "المستخدمين";
$lang["primary_contact"] = "اتصال رئيسي";
$lang["disable_login"] = "تعطيل تسجيل الدخول";
$lang["disable_login_help_message"] = "لن يكون المستخدم قادرا على تسجيل الدخول في هذا النظام!";
$lang["email_login_details"] = "تفاصيل تسجيل الدخول بالبريد الإلكتروني إلى هذا المستخدم";
$lang["generate"] = "إنشاء";
$lang["show_text"] = "عرض النص";
$lang["hide_text"] = "إخفاء النص";
$lang["mark_as_inactive"] = "وضع علامة كغير نشط";
$lang["mark_as_inactive_help_message"] = "لن يتمكن المستخدمون غير النشطين من تسجيل الدخول في هذا النظام ولا يتم احتسابهم في قائمة المستخدمين النشطين!";

$lang["invoice_id"] = "المعرف";
$lang["payments"] = "الدفعات";
$lang["invoice_sent_message"] = "تم إرسال الفاتورة!";
$lang["attached"] = "المرفق";
$lang["vat_number"] = "رقم ضريبة القيمة المضافة";
$lang["invite_an_user"] = " دعوة مستخدم ل %s"; // Invite an user for {company name}
$lang["unit_type"] = "نوع الوحدة";

/* projects */
$lang["add_project"] = "إضافة مشروع";
$lang["edit_project"] = "تعديل المشروع";
$lang["delete_project"] = "حذف المشروع";
$lang["project"] = "مشروع";
$lang["projects"] = "المشاريع";
$lang["all_projects"] = "جميع المشاريع";
$lang["member"] = "عضو";
$lang["overview"] = "نظرة عامة";
$lang["project_members"] = "أعضاء المشروع";
$lang["add_member"] = "إضافة عضو";
$lang["delete_member"] = "حذف العضو";
$lang["start_timer"] = "بدء الموقت";
$lang["stop_timer"] = "إيقاف مؤقت";
$lang["project_timeline"] = "الجدول الزمني للمشروع";
$lang["open_projects"] = "المشاريع المفتوحة";
$lang["projects_completed"] = "المشاريع المنجزة";
$lang["progress"] = "التقدم";
$lang["activity"] = "نشاط";
$lang["started_at"] = "بدأت في";
$lang["customer_feedback"] = "ملاحظات العملاء";
$lang["project_comment_reply"] = "رد تعليق المشروع";
$lang["task_comment_reply"] = "رد تعليق على مهمة";
$lang["file_comment_reply"] = "رد تعليق على ملف ";
$lang["customer_feedback_reply"] = "Customer feedback reply";

/* expense */
$lang["add_category"] = "إضافة فئة";
$lang["edit_category"] = "تعديل الفئة";
$lang["delete_category"] = "حذف الفئة";
$lang["category"] = "الفئة";
$lang["categories"] = "الفئات";
$lang["expense_categories"] = "فئات المصاريف";
$lang["add_expense"] = "إضافة حساب";
$lang["edit_expense"] = "تعديل الحساب";
$lang["delete_expense"] = "حذف حساب";
$lang["expense"] = "المصاريف";
$lang["expenses"] = "المصاريف";
$lang["date_of_expense"] = "تاريخ المصروفات";
$lang["finance"] = "المالية";

/* notes */
$lang["add_note"] = "اضف ملاحظة";
$lang["edit_note"] = "تعديل الملاحظة";
$lang["delete_note"] = "حذف الملاحظة";
$lang["note"] = "ملاحظة";
$lang["notes"] = "ملاحظات";
$lang["sticky_note"] = "ملاحظة مثبتة (خاص)";

/* history */
$lang["history"] = "History";

/* timesheet */
$lang["timesheets"] = "الجداول الزمنية";
$lang["log_time"] = "وقت التسجيل";
$lang["edit_timelog"] = "تعديل وقت التسجيل";
$lang["delete_timelog"] = "حذف وقت التسجيلg";
$lang["timesheet_statistics"] = "إحصاءات الجدول الزمني";

/* milestones */
$lang["add_milestone"] = "إضافة مرحلة";
$lang["edit_milestone"] = "تعديل المرحلة";
$lang["delete_milestone"] = "حذف مرحلة";
$lang["milestone"] = "مرحلة";
$lang["milestones"] = "المراحل";

/* files */
$lang["add_files"] = "إضافة ملفات";
$lang["edit_file"] = "تعديل ملف";
$lang["delete_file"] = "حذف ملف";
$lang["file"] = "ملف";
$lang["files"] = "ملفات";
$lang["file_name"] = "اسم الملف";
$lang["size"] = "الحجم";
$lang["uploaded_by"] = "تم الرفع بواسطة";
$lang["accepted_file_format"] = "صيغة الملف المقبول";
$lang["comma_separated"] = "مفصولة بفواصل";
$lang["project_file"] = "ملف";
$lang["download"] = "تحميل";
$lang["download_files"] = "تنزيل ملفات %s"; //Ex. Download 4 files
$lang["file_preview_is_not_available"] = "معاينة الملف غير متوفرة.";

/* tasks */
$lang["add_task"] = "إضافة مهمة";
$lang["edit_task"] = "تعديل المهمة";
$lang["delete_task"] = "حذف المهمة";
$lang["task"] = "المهمة";
$lang["tasks"] = "المهام";
$lang["my_tasks"] = "مهامي";
$lang["my_open_tasks"] = "مهامي المفتوحة";
$lang["assign_to"] = "المكلف";
$lang["assigned_to"] = "المكلف";
$lang["labels"] = "تسميات";
$lang["to_do"] = "مهام جديدة";
$lang["in_progress"] = "مهام قيد العمل";
$lang["done"] = "مهام منجزة";
$lang["task_info"] = "معلومات المهمة";
$lang["points"] = "درجة الأهمية";
$lang["point"] = "نقطة";
$lang["task_status"] = "حالة المهمة";

/* comments */
$lang["comment"] = "تعليق";
$lang["comments"] = "تعليقات";
$lang["write_a_comment"] = "أكتب تعليقا...";
$lang["write_a_reply"] = "اكتب رد ...";
$lang["post_comment"] = "أضف تعليقا";
$lang["post_reply"] = "إضافة رد";
$lang["reply"] = "الرد";
$lang["replies"] = "الردود";
$lang["like"] = "اعجاب";
$lang["unlike"] = "عدم إعجاب";
$lang["view"] = "عرض";
$lang["project_comment"] = "تعليق على مشروع";
$lang["task_comment"] = "تعليق  على مهمة";
$lang["file_comment"] = "تعليق على ملف";

/* time format */
$lang["today"] = "اليوم";
$lang["yesterday"] = "أمس";
$lang["tomorrow"] = "غدا";

$lang["today_at"] = "اليوم في";
$lang["yesterday_at"] = "بالأمس في
";

/* tickets */

$lang["add_ticket"] = "إضافة تذكرة";
$lang["ticket"] = "تذكرة";
$lang["tickets"] = "تذاكر";
$lang["ticket_id"] = "رقم التذكرة";
$lang["client_replied"] = "إجابة العميل";
$lang["change_status"] = "تغيير الحالة";
$lang["last_activity"] = "آخر نشاط";
$lang["open_tickets"] = "فتح التذاكر";
$lang["ticket_status"] = "حالة التذكرة";

/* ticket types */

$lang["add_ticket_type"] = "إضافة نوع تذكرة";
$lang["ticket_type"] = "نوع التذكرة";
$lang["ticket_types"] = "أنواع التذاكر";
$lang["edit_ticket_type"] = "تعديل نوع التذكرة";
$lang["delete_ticket_type"] = "حذف نوع التذكرة";

/* payment methods */

$lang["add_payment_method"] = "إضافة طريقة دفع";
$lang["payment_method"] = "طريقة الدفع";
$lang["payment_methods"] = "طرق الدفع";
$lang["edit_payment_method"] = "تعديل طريقة الدفع";
$lang["delete_payment_method"] = "حذف طريقة الدفع";

/* invoices */

$lang["add_invoice"] = "إضافة فاتورة";
$lang["edit_invoice"] = "تعديل الفاتورة";
$lang["delete_invoice"] = "حذف الفاتورة";
$lang["invoice"] = "فاتورة";
$lang["invoices"] = "الفواتير";
$lang["bill_date"] = "تاريخ الفاتورة";
$lang["due_date"] = "الموعد المحدد";
$lang["payment_date"] = "تاريخ الدفع";
$lang["bill_to"] = "فاتورة الى";
$lang["invoice_value"] = "قيمة الفاتورة";
$lang["payment_received"] = "الدفعات المستلمة";
$lang["invoice_payments"] = "الدفعات";
$lang["draft"] = "مشروع";
$lang["fully_paid"] = "مدفوعة بالكامل";
$lang["partially_paid"] = "مدفوعة جزئيا";
$lang["not_paid"] = "لم تدفع";
$lang["overdue"] = "متأخر";
$lang["invoice_items"] = "بنود الفاتورة";
$lang["edit_invoice"] = "تعديل الفاتورة";
$lang["delete_invoice"] = "حذف الفاتورة";
$lang["item"] = "بند";
$lang["add_item"] = "اضافة بند";
$lang["create_new_item"] = "إنشاء عنصر جديد";
$lang["select_or_create_new_item"] = "حدد من القائمة أو أنشئ بند جديد ...";
$lang["quantity"] = "الكمية";
$lang["rate"] = "المبلغ";
$lang["total_of_all_pages"] = "إجمالي كل الصفحات";
$lang["sub_total"] = "المجموع";
$lang["total"] = "المبلغ الإجمالي";
$lang["last_email_sent"] = "آخر رسالة إلكترونية أرسلت";
$lang["item_library"] = "مكتبة البند";
$lang["add_payment"] = "إضافة دفعة";
$lang["never"] = "أبدا";
$lang["email_invoice_to_client"] = "إرسال فاتورة بالبريد الإلكتروني إلى العميل";
$lang["download_pdf"] = "تحميل PDF";
$lang["print"] = "طباعة";
$lang["actions"] = "الأعمال";
$lang["balance_due"] = "المبلغ المستحق";
$lang["paid"] = "المبلغ المدفوع";
$lang["amount"] = "المبلغ";
$lang["invoice_payment_list"] = "قائمة دفع الفواتير";
$lang["invoice_statistics"] = "إحصاءات الفاتورة";
$lang["payment"] = "دفع";

/* email templates */
$lang["email_templates"] = "قوالب البريد الإلكتروني";
$lang["select_a_template"] = "حدد نموذجا للتعديل";
$lang["avilable_variables"] = "المتغيرات المتاحة";
$lang["restore_to_default"] = "العودة إلى الإفتراضي";
$lang["template_restored"] = "تمت اعادة النموذج إلى الوضع الافتراضي.";
$lang["login_info"] = "معلومات تسجيل الدخول";
$lang["reset_password"] = "إعادة تعيين كلمة المرور";
$lang["team_member_invitation"] = "دعوة عضو الفريق";
$lang["client_contact_invitation"] = "دعوة جهة اتصال العميل";
$lang["send_invoice"] = "إرسال الفاتورة";
$lang["signature"] = "التوقيع";

/* roles */

$lang["role"] = "الصلاحية";
$lang["roles"] = "الصلاحيات";
$lang["add_role"] = "إضافة صلاحية";
$lang["edit_role"] = "تعديل الصلاحية";
$lang["delete_role"] = "حذف الصلاحية";
$lang["use_seetings_from"] = "استخدام الإعدادات من";
$lang["permissions"] = "صلاحيات";
$lang["yes_all_members"] = "نعم، جميع الأعضاء";
$lang["yes_specific_members_or_teams"] = "نعم، أعضاء أو فرق محددة";
$lang["yes_specific_ticket_types"] = "نعم، أنواع التذاكر محددة";
$lang["select_a_role"] = "حدد الصلاحية";
$lang["choose_members_and_or_teams"] = "اختر أعضاء و / أو فرق";
$lang["choose_ticket_types"] = "اختر أنواع التذاكر";
$lang["excluding_his_her_time_cards"] = "باستثناء بطاقات الوقت الخاصة به";
$lang["excluding_his_her_leaves"] = "باستثناء بطاقات الوقت الخاصة به";
$lang["can_manage_team_members_leave"] = "يمكنه إدارة إجازات أعضاء الفريق؟";
$lang["can_manage_team_members_timecards"] = "يمكنه إدارة  بطاقات وقت أعضاء الفريق؟";
$lang["can_access_invoices"] = " يمكنه الوصول إلى الفواتير؟";
$lang["can_access_expenses"] = "يمكنه الوصول إلى المصاريف؟";
$lang["can_access_clients_information"] = "هل يمكنه الدخول إلى معلومات العميل؟";
$lang["can_access_tickets"] = "يمكنه الوصول إلى التذاكر؟";
$lang["can_manage_announcements"] = "يمكنه إدارة الإعلانات؟";

/* timeline */
$lang["post_placeholder_text"] = "شارك بفكرة أو مرفقات ...";
$lang["post"] = "أرسل";
$lang["timeline"] = "النقاشات العامة";
$lang["load_more"] = "تحميل المزيد";
$lang["upload_file"] = "رفع الملف";
$lang["upload"] = "رفع";
$lang["new_posts"] = "مشاركات جديدة";

/* taxes */

$lang["add_tax"] = "إضافة ضريبة";
$lang["tax"] = "ضريبة";
$lang["taxes"] = "الضرائب";
$lang["edit_tax"] = "تعديل الضريبة";
$lang["delete_tax"] = "حذف الضريبة";
$lang["percentage"] = "النسبة المئوية (٪)";
$lang["second_tax"] = "الضريبة الثانية";

/* Version 1.2 */
$lang["available_on_invoice"] = "متوفر على الفاتورة";
$lang["available_on_invoice_help_text"] = "ستظهر طريقة الدفع في فواتير العميل.";
$lang["minimum_payment_amount"] = "الحد الأدنى لمبلغ الدفعة";
$lang["minimum_payment_amount_help_text"] = "لن يتمكن العملاء من دفع الفاتورة باستخدام طريقة الدفع هذه، إذا كانت قيمة الفاتورة أقل من هذه القيمة.";
$lang["pay_invoice"] = "دفع الفاتورة";
$lang["pay_button_text"] = "نص زر الدفع";
$lang["minimum_payment_validation_message"] = "لا يمكن أن يكون مبلغ الدفعة أقل من:"; //ex. The payment amount can't be less then: USD 100.00
$lang["invoice_settings"] = "إعدادات الفاتورة";
$lang["allow_partial_invoice_payment_from_clients"] = "السماح بالدفع الجزئي من العملاء";
$lang["invoice_color"] = "لون الفاتورة";
$lang["invoice_footer"] = "ذيل الفاتورة";
$lang["invoice_preview"] = "معاينة الفاتورة";
$lang["close_preview"] = "إغلاق المعاينة";
$lang["only_me"] = "انا فقط";
$lang["specific_members_and_teams"] = "أعضاء وفرق محددة";
$lang["rows_per_page"] = "الصفوف في كل صفحة";
$lang["price"] = "السعر";
$lang["security_type"] = "نوع الأمان";

$lang["client_can_view_tasks"] = "يمكن للعميل عرض المهام؟";
$lang["client_can_create_tasks"] = "يمكن للعميل إنشاء المهام؟";
$lang["client_can_edit_tasks"] = "يمكن للعميل تعديل المهام؟";
$lang["client_can_comment_on_tasks"] = "يمكن للعميل التعليق على المهام؟";

$lang["set_project_permissions"] = "تعيين صلاحيات المشروع";
$lang["can_create_projects"] = "يمكن إنشاء المشاريع";
$lang["can_edit_projects"] = "يمكن تعديل المشاريع";
$lang["can_delete_projects"] = "يمكن حذف المشاريع";
$lang["can_create_tasks"] = "يمكن إنشاء المهام";
$lang["can_edit_tasks"] = "يمكن تعديل المهام";
$lang["can_delete_tasks"] = "يمكن حذف المهام";
$lang["can_comment_on_tasks"] = "يمكن التعليق على المهام";
$lang["can_create_milestones"] = "يمكن إنشاء مراحل";
$lang["can_edit_milestones"] = "يمكن تعديل المراحل";
$lang["can_delete_milestones"] = "يمكن حذف المراحل";
$lang["can_add_remove_project_members"] = "يمكن إضافة / حذف عضاء المشروع";
$lang["can_delete_files"] = "يمكن حذف الملفات";

/* Version 1.2.2 */
$lang["label"] = "تسمية";
$lang["send_bcc_to"] = "عند إرسال فاتورة إلى العميل، أرسل BCC إلى";
$lang["mark_project_as_completed"] = "وضع المشروع في حالة منتهي";
$lang["mark_project_as_canceled"] = "وضع المشروع في حالة ملغي";
$lang["mark_project_as_open"] = "وضع المشروع في حالة مفتوح";

/* Version 1.3 */
$lang["notification"] = "الإشعارات";
$lang["notifications"] = "الإشعارات";
$lang["notification_settings"] = "إعدادات الإشعار";
$lang["enable_email"] = "بالبريد الإلكتروني";
$lang["enable_web"] = "على البرنامج";
$lang["event"] = "حدث";
$lang["notify_to"] = "لمن سيصل الإشعار";

$lang["project_created"] = "تم إنشاء المشروع";
$lang["project_deleted"] = "تم حذف المشروع";
$lang["project_task_created"] = "تم إنشاء مهمة المشروع";
$lang["project_task_updated"] = "تم تحديث مهمة المشروع";
$lang["project_task_assigned"] = "تم تعيين مهمة المشروع ";
$lang["project_task_started"] = "بدأت مهمة المشروع";
$lang["project_task_finished"] = "انتهت مهمة المشروع";
$lang["project_task_reopened"] = "أعيد فتح مهمة المشروع";
$lang["project_task_deleted"] = "تم حذف مهمة المشروع";
$lang["project_task_commented"] = "تم التعليق على مهمة المشروع ";
$lang["project_member_added"] = "إضافة عضو المشروع";
$lang["project_member_deleted"] = "تم حذف عضو المشروع";
$lang["project_file_added"] = "تمت إضافة ملف المشروع";
$lang["project_file_deleted"] = "تم حذف ملف المشروع";
$lang["project_file_commented"] = "تم تعليق ملف المشروع";
$lang["project_comment_added"] = "تمت إضافة تعليق المشروع";
$lang["project_comment_replied"] = "الرد على  تعليق المشروع";
$lang["project_customer_feedback_added"] = "تمت إضافة تعليقات العملاء على المشروع";
$lang["project_customer_feedback_replied"] = "ردود فعل العملاء في المشروع";
$lang["client_signup"] = "اشتراك العميل";
$lang["invoice_online_payment_received"] = "تم استلام الفاتورة عبر الإنترنت";
$lang["leave_application_submitted"] = "تم إرسال الإجازة";
$lang["leave_approved"] = "تمت الموافقة على الإجازة";
$lang["leave_assigned"] = "تم تعيين الإجازة";
$lang["leave_rejected"] = "تم رفض الإجازة";
$lang["leave_canceled"] = "تم إلغاء الإجازة";
$lang["ticket_created"] = "تم إنشاء تذكرة";
$lang["ticket_commented"] = "تم تعليق التذكرة";
$lang["ticket_closed"] = "تم إتمام التذاكر";
$lang["ticket_reopened"] = "تم إعادة فتح التذكرة";
$lang["leave"] = "إجازة";

$lang["client_primary_contact"] = "جهة الاتصال الأساسية للعميل";
$lang["client_all_contacts"] = "جميع جهات الاتصال من العميل";
$lang["task_assignee"] = "المكلف بالمهمة";
$lang["task_collaborators"] = "المساعدون في المهام";
$lang["comment_creator"] = "منشئ التعليقات";
$lang["leave_applicant"] = "إجازة طالب وظيفة";
$lang["ticket_creator"] = "منشئ التذاكر";

$lang["no_new_notifications"] = "لا توجد إشعارات.";

/* Notification messages */

$lang["notification_project_created"] = "إنشاء مشروع جديد.";
$lang["notification_project_deleted"] = "حذف مشروع.";
$lang["notification_project_task_created"] = "إنشاء مهمة جديدة.";
$lang["notification_project_task_updated"] = "تحديث مهمة.";
$lang["notification_project_task_assigned"] = "تم تعيين مهمة إلى %s"; //Assigned a task to Mr. X
$lang["notification_project_task_started"] = "بدء مهمة.";
$lang["notification_project_task_finished"] = "أنهى مهمة.";
$lang["notification_project_task_reopened"] = "أعيد فتح مهمة.";
$lang["notification_project_task_deleted"] = "حذف مهمة.";
$lang["notification_project_task_commented"] = "علق على مهمة.";
$lang["notification_project_member_added"] = "تمت إضافة %s في المشروع."; //Added Mr. X in a project.
$lang["notification_project_member_deleted"] = "تم حذف %s من أحد المشاريع."; //Deleted Mr. X from a project.
$lang["notification_project_file_added"] = "تمت إضافة ملف في المشروع.";
$lang["notification_project_file_deleted"] = "حذف ملف من المشروع.";
$lang["notification_project_file_commented"] = "علق على ملف.";
$lang["notification_project_comment_added"] = "علق على مشروع.";
$lang["notification_project_comment_replied"] = "الرد على تعليق المشروع.";
$lang["notification_project_customer_feedback_added"] = "علق على مشروع.";
$lang["notification_project_customer_feedback_replied"] = "أجاب على تعليق.";
$lang["notification_client_signup"] = "التوقيع كعميل جديد."; //Mr. X signed up as a new client.
$lang["notification_invoice_online_payment_received"] = "تم إرسال دفعة عبر الإنترنت.";
$lang["notification_leave_application_submitted"] = "تقديم طلب الإجازة.";
$lang["notification_leave_approved"] = "الموافقة على إجازة %s."; //Approve a leave of Mr. X
$lang["notification_leave_assigned"] = "عين إجازة إلى %s."; //Assigned a leave to Mr. X
$lang["notification_leave_rejected"] = "رفض إجازة %s."; //Approve a leave of Mr. X
$lang["notification_leave_canceled"] = "إلغاء تطبيق الإجازة.";
$lang["notification_ticket_created"] = "إنشاء تذكرة جديدة.";
$lang["notification_ticket_commented"] = "علق على تذكرة.";
$lang["notification_ticket_closed"] = "أغلقت التذكرة.";
$lang["notification_ticket_reopened"] = "أعيد فتح التذكرة.";

$lang["general_notification"] = "إشعار عام";

$lang["disable_online_payment"] = "تعطيل الدفع عبر الإنترنت";
$lang["disable_online_payment_description"] = "إخفاء خيارات الدفع عبر الإنترنت في الفاتورة لهذا العميل.";

$lang["client_can_view_project_files"] = "يمكن للعميل عرض ملفات المشروع؟";
$lang["client_can_add_project_files"] = "يمكن للعميل إضافة ملفات المشروع؟";
$lang["client_can_comment_on_files"] = "يمكن للعميل التعليق على الملفات؟";
$lang["mark_invoice_as_not_paid"] = "تفعيل كغير مدفوع"; //Change invoice status to Not Paid

$lang["set_team_members_permission"] = "تعيين صلاحيات أعضاء الفريق";
$lang["can_view_team_members_contact_info"] = "يمكن عرض معلومات اتصال عضو الفريق؟";
$lang["can_view_team_members_social_links"] = "يمكن عرض الروابط الاجتماعية لأعضاء الفريق؟";

$lang["collaborator"] = "مساعد";
$lang["collaborators"] = "المساعدون";

/* Version 1.4 */

$lang["modules"] = "الوحدات";
$lang["manage_modules"] = "إدارة الوحدات";
$lang["module_settings_instructions"] = "تحديد الوحدات التي تريد استخدامها.";

$lang["task_point_help_text"] = "تساعد على تحديد نسبة إكتمال المشروع"; //meaning of task point

$lang["mark_as_open"] = "وضع علامة على أنها مفتوحة";
$lang["mark_as_closed"] = "وضع علامة على أنها تمت";

$lang["ticket_assignee"] = "تذكرة موكلة إلى";

$lang["estimate"] = "التسعيرة";
$lang["estimates"] = "الطلبات";
$lang["estimate_request"] = "طلب تسعيرة";
$lang["estimate_requests"] = "الطلبات الواردة";
$lang["estimate_list"] = "قائمة الأسعار";
$lang["estimate_forms"] = "نماذج الطلبات";
$lang["estimate_request_forms"] = "نماذج الطلبات";

$lang["add_form"] = "إضافة نموذج";
$lang["edit_form"] = "تعديل النموذج";
$lang["delete_form"] = "حذف النموذج";

$lang["add_field"] = "إضافة حقل";
$lang["placeholder"] = "العنصر النائب";
$lang["required"] = "إجباري";

$lang["field_type"] = "نوع الحقل";
$lang["preview"] = "عرض";

$lang["field_type_text"] = "نص";
$lang["field_type_textarea"] = "مساحة النص";
$lang["field_type_select"] = "إختيار";
$lang["field_type_multi_select"] = "متعدد الإختيارت";

$lang["request_an_estimate"] = "طلب تسعيرة";
$lang["estimate_submission_message"] = "تم تقديم طلبك بنجاح!";

$lang["hold"] = "معلق";
$lang["processing"] = "تتم معالجته";
$lang["estimated"] = "يتم تقديره";

$lang["add_estimate"] = "إضافة تسعيرة";
$lang["edit_estimate"] = "تعديل التسعيرة";
$lang["delete_estimate"] = "حذف التسعيرة";
$lang["valid_until"] = "صالحة حتى";
$lang["estimate_date"] = "التاريخ المتوقع";
$lang["accepted"] = "مقبول";
$lang["declined"] = "رفض";
$lang["sent"] = "أرسلت";
$lang["estimate_preview"] = "معاينة التقدير";
$lang["estimate_to"] = "تقدير إلى";

$lang["can_access_estimates"] = "يمكن الوصول إلى قوائم الأسعار؟";
$lang["request_an_estimate"] = "طلب تقدير";
$lang["estimate_request_form_selection_title"] = "يرجى تحديد نموذج من القائمة التالية لإرسال طلبك.";

$lang["mark_as_processing"] = "تفعيل كمعالج";
$lang["mark_as_estimated"] = "تفعيل كمقدر";
$lang["mark_as_hold"] = "تفعيل كمعلق";
$lang["mark_as_canceled"] = "تفعيل كملغى";

$lang["mark_as_sent"] = "تفعيل كمرسل";
$lang["mark_as_accepted"] = "تفعيل كمقبول";
$lang["mark_as_rejected"] = "تفعيل كمرفوض";
$lang["mark_as_declined"] = "تفعيل كمرفوض";

$lang["estimate_request_received"] = "تم تلقي طلب التقدير";
$lang["estimate_sent"] = "تم إرسال التقدير";
$lang["estimate_accepted"] = "تم قبول التقدير";
$lang["estimate_rejected"] = "تم رفض التقدير";

$lang["notification_estimate_request_received"] = "قدم طلب تقدير";
$lang["notification_estimate_sent"] = "تم إرسال تقدير";
$lang["notification_estimate_accepted"] = "تم قبول تقدير";
$lang["notification_estimate_rejected"] = "تم رفض تقدير";

$lang["clone_project"] = "استنساخ المشروع";
$lang["copy_tasks"] = "نسخ المهام";
$lang["copy_project_members"] = "نسخ أعضاء المشروع";
$lang["copy_milestones"] = "نسخ المراحل";
$lang["copy_same_assignee_and_collaborators"] = "نسخ نفس المكلف و المساعدين";
$lang["copy_tasks_start_date_and_deadline"] = "نسخ تاريخ بدء المهام والمواعيد النهائية";
$lang["task_comments_will_not_be_included"] = "لن يتم تضمين تعليقات المهام";
$lang["project_cloned_successfully"] = "تم استنساخ المشروع بنجاح";

$lang["search"] = "بحث";
$lang["no_record_found"] = "لم يتم العثور على أي سجل.";
$lang["excel"] = "Excel";
$lang["print_button_help_text"] = "اضغط زر الخروج عند الانتهاء.";
$lang["are_you_sure"] = "هل أنت متأكد؟";
$lang["file_upload_instruction"] = "يمكنك سحب الملفات وإفلاتها هنا <br/> (أو النقر لتصفح ...)";
$lang["file_name_too_long"] = "اسم الملف طويل جدا.";
$lang["scrollbar"] = "شريط التمرير";

$lang["short_sunday"] = "أح";
$lang["short_monday"] = "إث";
$lang["short_tuesday"] = "ث";
$lang["short_wednesday"] = "إر";
$lang["short_thursday"] = "خ";
$lang["short_friday"] = "ج";
$lang["short_saturday"] = "س";

$lang["min_sunday"] = "أح";
$lang["min_monday"] = "إث";
$lang["min_tuesday"] = "ث";
$lang["min_wednesday"] = "إر";
$lang["min_thursday"] = "خ";
$lang["min_friday"] = "ج";
$lang["min_saturday"] = "س";

$lang["january"] = "يناير";
$lang["february"] = "فبراير";
$lang["march"] = "مارس";
$lang["april"] = "أبريل";
$lang["may"] = "مايو";
$lang["june"] = "يونيو";
$lang["july"] = "يوليو";
$lang["august"] = "أغسطس";
$lang["september"] = "سبتمبر";
$lang["october"] = "أكتوبر";
$lang["november"] = "نوفمبر";
$lang["december"] = "ديسمبر";

$lang["short_january"] = "يان";
$lang["short_february"] = "فبر";
$lang["short_march"] = "مار";
$lang["short_april"] = "ابر";
$lang["short_may"] = "ماي";
$lang["short_june"] = "يون";
$lang["short_july"] = "يول";
$lang["short_august"] = "أغس";
$lang["short_september"] = "سبت";
$lang["short_october"] = "أكت";
$lang["short_november"] = "نوف";
$lang["short_december"] = "دسم";

/* Version 1.5 */

$lang["no_such_file_or_directory_found"] = "لا يوجد ملف أو مجلد.";
$lang["gantt"] = "مخطط المشروع";
$lang["not_specified"] = "غير محدد";
$lang["group_by"] = "الفلتر";
$lang["create_invoice"] = "إنشاء فاتورة";
$lang["include_all_items_of_this_estimate"] = "تشمل جميع العناصر من هذا التقدير";
$lang["edit_payment"] = "تعديل الدفع";
$lang["disable_client_login"] = "تعطيل تسجيل دخول العميل";
$lang["disable_client_signup"] = "تعطيل اشتراك العميل";

$lang["chart"] = "التخطيط";
$lang["signin_page_background"] = "تسجيل الدخول إلى صفحة الخلفية";
$lang["show_logo_in_signin_page"] = "عرض الشعار في صفحة تسجيل الدخول";
$lang["show_background_image_in_signin_page"] = "عرض صورة الخلفية في صفحة تسجيل الدخول";

/* Version 1.6 */

$lang["more"] = "أكثر";
$lang["custom"] = "مخصص";
$lang["clear"] = "مسح";
$lang["expired"] = "منتهي الصلاحية";
$lang["enable_attachment"] = "تفعيل المرفقات";
$lang["custom_fields"] = "الحقول المخصصة";
$lang["edit_field"] = "تعديل الحقل";
$lang["delete_field"] = "حذف الحقل";
$lang["client_info"] = "معلومات العميل";
$lang["edit_expenses_category"] = "تعديل فئة النفقات";
$lang["eelete_expenses_category"] = "حذف فئة النفقات";
$lang["empty_starred_projects"] = "للوصول إلى المشاريع المفضلة لديك بسرعة، يرجى الذهاب إلى عرض المشروع ووضع علامة على النجمة.";
$lang["empty_starred_clients"] = "للوصول إلى العملاء المفضلة لديك بسرعة، يرجى الذهاب إلى عرض العميل وعلامة النجمة.";
$lang["download_zip_name"] = "وثائق";
$lang["invoice_prefix"] = "إختصار الفاتورة";
$lang["invoice_style"] = "نوع الفاتورة";
$lang["delete_confirmation_message"] = " هل أنت واثق؟ لن تتمكن من التراجع عن هذا الإجراء!";
$lang["left"] = "اليسار";
$lang["right"] = "اليمين";
$lang["currency_position"] = "وضع العملة";
$lang["recipient"] = "مستلم";

$lang["new_message_sent"] = "تم إرسال رسالة جديدة";
$lang["message_reply_sent"] = "الإجابة على رسالة";
$lang["notification_new_message_sent"] = "أرسل رسالة.";
$lang["notification_message_reply_sent"] = "الإجابة على رسالة";
$lang["invoice_payment_confirmation"] = "تأكيد دفع الفاتورة";
$lang["notification_invoice_payment_confirmation"] = "تم استلام الدفعة";

/* Version 1.7 */

$lang["client_can_create_projects"] = "يمكن للعميل إنشاء المشاريع؟";
$lang["client_can_view_timesheet"] = "يمكن للعميل عرض الجدول الزمني؟";
$lang["client_can_view_gantt"] = "يمكن للعميل عرض غانت؟";
$lang["client_can_view_overview"] = "يمكن للعميل عرض نظرة عامة على المشروع؟";
$lang["client_can_view_milestones"] = "يمكن للعميل عرض المراحل؟";

$lang["items"] = "البنود";
$lang["edit_item"] = "تعديل البند";
$lang["item_edit_instruction"] = "ملاحظة: لن تؤثر التغييرات على الفواتير أو التقديرات الحالية.";

$lang["recurring"] = "متكرر";
$lang["repeat_every"] = "تكرار كل"; //Ex. repeat every 2 months
$lang["interval_days"] = "أيام";
$lang["interval_weeks"] = "أسبوع";
$lang["interval_months"] = "الشهور";
$lang["interval_years"] = "سنوات";
$lang["cycles"] = "دورات";
$lang["recurring_cycle_instructions"] = "سيتم إيقاف التكرار بعد عدد من الدورات. يبقيه فارغا للانهائية.";
$lang["next_recurring_date"] = "التكرار التالي";
$lang["stopped"] = "توقفت";
$lang["past_recurring_date_error_message_title"] = "يرجع تاريخ الفاتورة المحدد ونوع التكرار إلى تاريخ سابق.";
$lang["past_recurring_date_error_message"] = "يجب أن يكون التاريخ المتكرر التالي تاريخا مستقبلا. يرجى إدخال تاريخ لاحق.";
$lang["sub_invoices"] = "الفواتير الفرعية";

$lang["cron_job_required"] = "الكرون إجباري في هذا العمل.";

$lang["recurring_invoice_created_vai_cron_job"] = "الفاتورة المتكررة التي تم إنشاؤها عبر الكرون.";
$lang["notification_recurring_invoice_created_vai_cron_job"] = "تم إنشاء فاتورة جديدة";

$lang["field_type_number"] = "رقم";
$lang["show_in_table"] = "إظهار في الجدول";
$lang["show_in_invoice"] = "تظهر في الفاتورة";
$lang["visible_to_admins_only"] = "مرئي للمشرفين فقط";
$lang["hide_from_clients"] = "إخفاء من العملاء";
$lang["public"] = "عامة";

$lang["help"] = "المساعدة";
$lang["articles"] = "المقالات";
$lang["add_article"] = "إضافة مقالة جديدة";
$lang["edit_article"] = "تعديل المقالة";
$lang["delete_article"] = "حذف المقالة";
$lang["can_manage_help_and_knowledge_base"] = "يمكن إدارة المساعدة و عن الشركة?";

$lang["how_can_we_help"] = "كيف يمكن أن نساعد؟";
$lang["help_page_title"] = "ويكي الداخلي";
$lang["search_your_question"] = "ابحث عن سؤالك";
$lang["no_result_found"] = "لم يتم العثور على نتائج.";
$lang["sort"] = "فلترة";
$lang["total_views"] = "عدد المشاهدات";

$lang["help_and_support"] = "المساعدة";
$lang["knowledge_base"] = "عن الشركة";

$lang["payment_success_message"] = "اكتملت عملية الدفع.";
$lang["payment_card_charged_but_system_error_message"] = "قد يتم تحصيل رسوم من البطاقة ولكن لا يمكننا إكمال العملية. يرجى الاتصال بمسؤول النظام.";
$lang["card_payment_failed_error_message"] = "لا يمكننا معالجة دفعتك في الوقت الحالي، لذا يرجى إعادة المحاولة لاحقا.";

$lang["message_received"] = "تم تلقي الرسالة";
$lang["in_number_of_days"] = "خلال %s من الأيام"; //Ex. In 7 days
$lang["details"] = "تفاصيل";
$lang["summary"] = "ملخص";
$lang["project_timesheet"] = "الجدول الزمني للمشروع";

$lang["set_event_permissions"] = "تعيين صلاحيات الحدث";
$lang["disable_event_sharing"] = "تعطيل مشاركة الحدث";
$lang["can_update_team_members_general_info_and_social_links"] = "يمكنه تحديث المعلومات العامة والروابط الاجتماعية لأعضاء الفريق؟";
$lang["can_manage_team_members_project_timesheet"] = "يمكنه إدارة الجدول الزمني لمشاريع أعضاء الفريق؟";

$lang["cron_job"] = "رسائل التذكير";
$lang["cron_job_link"] = "رابط رسائل التذكير";
$lang["last_cron_job_run"] = "آخر تشغيل لرسائل التذكير";
$lang["created_from"] = "Created from"; //Ex. Created from Invoice#1
$lang["recommended_execution_interval"] = "فترة التنفيذ الموصى بها";

/* Version 1.8 */

$lang["integration"] = "الإدماج";
$lang["get_your_key_from_here"] = "احصل على مفتاحك من هنا:";
$lang["re_captcha_site_key"] = "مفتاح الموقع";
$lang["re_captcha_secret_key"] = "مفتاح سري";

$lang["re_captcha_error-missing-input-secret"] = "سر reCAPTCHA مفقود";
$lang["re_captcha_error-invalid-input-secret"] = "سر reCAPTCHA غير صالح.";
$lang["re_captcha_error-missing-input-response"] = "يرجى تحديد reCAPTCHA.";
$lang["re_captcha_error-invalid-input-response"] = "إعدادات الاستجابة غير صالحة أو غير صحيحة.";
$lang["re_captcha_error-bad-request"] = "الطلب غير صالح أو غير صحيح.";
$lang["re_captcha_expired"] = "انتهت صلاحية reCAPTCHA. رجاء أعد تحميل الصفحة.";

$lang["yes_all_tickets"] = "نعم، جميع التذاكر";
$lang["choose_ticket_types"] = "اختر أنواع التذاكر";

$lang["can_manage_all_projects"] = "يمكن إدارة جميع المشاريع";
$lang["show_most_recent_ticket_comments_at_the_top"] = "تظهر أحدث تعليقات التذكرة في الجزء العلوي";

$lang["new_event_added_in_calendar"] = "تمت إضافة تقويم جديد";
$lang["notification_new_event_added_in_calendar"] = "تمت إضافة حدث جديد.";

$lang["todo"] = "قائمة المهام";
$lang["add_a_todo"] = "إضافة مهمة ...";

/* Version 1.9 */

$lang["client_groups"] = "مجموعات العملاء";
$lang["add_client_group"] = "إضافة مجموعة عملاء";
$lang["edit_client_group"] = "تعديل مجموعة العملاء";
$lang["delete_client_group"] = "حذف مجموعة العملاء";

$lang["ticket_prefix"] = "اختصار التذكرة";
$lang["add_a_task"] = "إضافة مهمة ...";

$lang["add_task_status"] = "إضافة حالة المهمة";
$lang["edit_task_status"] = "تعديل حالة المهمة";
$lang["delete_task_status"] = "حذف حالة المهمة";

$lang["list"] = "قائمة";
$lang["kanban"] = "لوحة المهام";
$lang["priority"] = "الأولوي";
$lang["moved_up"] = "نقل";
$lang["moved_down"] = "انتقل إلى أسفل";
$lang["mark_project_as_hold"] = "وضع المشروع في حالة معلق";

$lang["repeat"] = "كرر";

$lang["hide_team_members_list"] = "إخفاء قائمة أعضاء الفريق؟";

$lang["task_list"] = "قائمة المهام";
$lang["task_kanban"] = "لوحة المهام";