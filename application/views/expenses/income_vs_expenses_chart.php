<div id="page-content" class="clearfix p20">
    <div class="panel clearfix">
        <ul data-toggle="ajax-tab" class="nav nav-tabs bg-white inner p10" role="tablist">
            <li class="title-tab"><h4 class="pl10"><?php echo lang("income_vs_expenses"); ?></h4></li>
            <div class="pull-right pr10">
                <div id="yearly-chart-date-range-selector">
                </div>
            </div>
        </ul>

        <div class="tab-content p15">
            <!--<div id="income-vs-expenses-chart" style="width: 100%; height: 350px;"></div>-->
            <script src="https://code.highcharts.com/highcharts.js"></script>
            <script src="https://code.highcharts.com/modules/exporting.js"></script>

            <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

            <script>
                var income = [[1,0],[2,0],[3,0],[4,0],[5,0],[6,0],[7,0],[8,0],[9,0],[10,0],[11,0],[12,0]];
                var expenses = [[1,0],[2,0],[3,0],[4,0],[5,0],[6,0],[7,0],[8,0],[9,0],[10,0],[11,0],[12,0]];
                var income_data  = [];
                var expenses_data  = [];
                $.ajax({
                    url: "<?php echo_uri("expenses/income_vs_expenses_chart_data") ?>",
                    data: [],
                    cache: false,
                    type: 'POST',
                    dataType: "json",
                    async: false,
                    success: function (response) {
                        income = response.income;
                        expenses = response.expenses;

                    }
                });
                $.each(income, function( index, value ) {
                    income_data.push(parseFloat((value+'').split(',')[1]));
                });
                $.each(expenses, function( index, value ) {
                    expenses_data.push(parseFloat((value+'').split(',')[1]));
                });
                Highcharts.chart('container', {
                    exporting: {
                        enabled: false
                    },
                    style:{
                        direction: 'rtl'
                    },
                    chart: {
                        type: 'areaspline'
                    },
                    title: {
                        text: ''
                    },
                    legend: {
                        layout: 'vertical',
                        floating: true,
                        align: 'left',
                        verticalAlign: 'top',
                        x: 90,
                        y: 45,
                        symbolPadding: 20,
                        symbolWidth: 50
                    },
                    xAxis: {
                        categories: [
                            "<?php echo lang('january'); ?> 1",
                            "<?php echo lang('february'); ?> 2",
                            "<?php echo lang('march'); ?> 3",
                            "<?php echo lang('april'); ?> 4",
                            "<?php echo lang('may'); ?> 5",
                            "<?php echo lang('june'); ?> 6",
                            "<?php echo lang('july'); ?> 7",
                            "<?php echo lang('august'); ?> 8",
                            "<?php echo lang('september'); ?> 9",
                            "<?php echo lang('october'); ?> 10",
                            "<?php echo lang('november'); ?> 11",
                            "<?php echo lang('december'); ?> 12"
                        ],
                        reversed: true,
                    },
                    yAxis: {
                        title:{
                        text:"المبالغ"
                        }
                    },
                    tooltip: {
                        shared: false,
                        formatter: function () {
                            return this.y ;
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    plotOptions: {
                        areaspline: {
                            fillOpacity: 0.5
                        }
                    },
                    series: [{
                        name: "الإيرادات",
                        data: income_data,
                        color: '#00BC94'
                    }, {
                        name: "المصروفات",
                        data: expenses_data,
                        color: '#FF5668'
                    }]
                });



            </script>
        </div>
    </div>
</div>

<?php
load_js(array(
    "assets/js/flot/jquery.flot.min.js",
    "assets/js/flot/jquery.flot.resize.min.js",
    "assets/js/flot/jquery.flot.tooltip.min.js",
    "assets/js/flot/curvedLines.js",
));
?>

<!--
<script type="text/javascript">
    var initIncomeExpenseChart = function (income, expense) {
        var dataset = [
            {
                data: income,
                color: "rgba(0, 179, 147, 1)",
                lines: {
                    show: true,
                    fill: 0.2
                },
                points: {
                    show: false
                },
                shadowSize: 0
            },
            {
                label: "<?php echo lang('income'); ?>",
                data: income,
                color: "rgba(0, 179, 147, 1)",
                lines: {
                    show: false
                },
                points: {
                    show: true,
                    fill: true,
                    radius: 4,
                    fillColor: "#fff",
                    lineWidth: 1
                },
                shadowSize: 0,
                curvedLines: {
                    apply: false
                }
            },
            {
                data: expense,
                color: "#F06C71",
                lines: {
                    show: true,
                    fill: 0.2
                },
                points: {
                    show: false
                },
                shadowSize: 0
            },
            {
                label: "<?php echo lang('expense'); ?>",
                data: expense,
                color: "#F06C71",
                lines: {
                    show: false
                },
                points: {
                    show: true,
                    fill: true,
                    radius: 4,
                    fillColor: "#fff",
                    lineWidth: 1
                },
                shadowSize: 0,
                curvedLines: {
                    apply: false
                }
            }

        ];
        $.plot("#income-vs-expenses-chart", dataset, {
            series: {
                curvedLines: {
                    apply: true,
                    active: true,
                    monotonicFit: true
                }
            },
            legend: {
                show: true
            },
            yaxis: {
                min: 0
            },
            xaxis: {
                ticks: [[1, "<?php echo lang('january'); ?> (1)"], [2, "<?php echo lang('february'); ?> (2)"], [3, "<?php echo lang('march'); ?> (3)"], [4, "<?php echo lang('april'); ?> (4)"], [5, "<?php echo lang('may'); ?> (5)"], [6, "<?php echo lang('june'); ?> (6)"], [7, "<?php echo lang('july'); ?> (7)"], [8, "<?php echo lang('august'); ?> (8)"], [9, "<?php echo lang('september'); ?> (9)"], [10, "<?php echo lang('october'); ?> (10)"], [11, "<?php echo lang('november'); ?> (11)"], [12, "<?php echo lang('december'); ?> (12)"]]
            },
            grid: {
                color: "#bbb",
                hoverable: true,
                borderWidth: 0,
                backgroundColor: '#FFF'
            },
            tooltip: {
                show: true,
                content: function (x, y, z) {
                    if (x) {
                        return "%s: " + toCurrency(z);
                    } else {
                        return false;
                    }
                },
                defaultTheme: false
            }
        });
    };
    var prepareExpensesFlotChart = function (data) {
        appLoader.show();
        $.ajax({
            url: "<?php echo_uri("expenses/income_vs_expenses_chart_data") ?>",
            data: data,
            cache: false,
            type: 'POST',
            dataType: "json",
            success: function (response) {
                appLoader.hide();
                initIncomeExpenseChart(response.income, response.expenses);
            }
        });
    };
    $(document).ready(function () {

        $("#yearly-chart-date-range-selector").appDateRange({
            dateRangeType: "yearly",
            onChange: function (dateRange) {
                prepareExpensesFlotChart(dateRange);
            },
            onInit: function (dateRange) {
                prepareExpensesFlotChart(dateRange);
            }
        });
    });
</script>
-->


