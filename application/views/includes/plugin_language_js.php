<script type="text/javascript">
    AppLanugage = {};
    AppLanugage.locale = "<?php echo lang('language_locale'); ?>";

    AppLanugage.days = <?php echo json_encode(array(lang("sunday"), lang("monday"), lang("tuesday"), lang("wednesday"), lang("thursday"), lang("friday"), lang("saturday")), JSON_UNESCAPED_UNICODE ); ?>;
    AppLanugage.daysShort = <?php echo json_encode(array(lang("short_sunday"), lang("short_monday"), lang("short_tuesday"), lang("short_wednesday"), lang("short_thursday"), lang("short_friday"), lang("short_saturday")), JSON_UNESCAPED_UNICODE ); ?>;
    AppLanugage.daysMin = <?php echo json_encode(array(lang("min_sunday"), lang("min_monday"), lang("min_tuesday"), lang("min_wednesday"), lang("min_thursday"), lang("min_friday"), lang("min_saturday")), JSON_UNESCAPED_UNICODE ); ?>;

    AppLanugage.months = <?php echo json_encode(array(lang("january")." (1)", lang("february")." (2)", lang("march")." (3)", lang("april")." (4)", lang("may")." (5)", lang("june")." (6)", lang("july")." (7)", lang("august")." (8)", lang("september")." (9)", lang("october")." (10)", lang("november")." (11)", lang("december")." (12)"), JSON_UNESCAPED_UNICODE ); ?>;
    AppLanugage.monthsShort = <?php echo json_encode(array(lang("january")." (1)", lang("february")." (2)", lang("march")." (3)", lang("april")." (4)", lang("may")." (5)", lang("june")." (6)", lang("july")." (7)", lang("august")." (8)", lang("september")." (9)", lang("october")." (10)", lang("november")." (11)", lang("december")." (12)"), JSON_UNESCAPED_UNICODE ); ?>;

    AppLanugage.today = "<?php echo lang('today'); ?>";
    AppLanugage.yesterday = "<?php echo lang('yesterday'); ?>";
    AppLanugage.tomorrow = "<?php echo lang('tomorrow'); ?>";

    AppLanugage.search = "<?php echo lang('search'); ?>";
    AppLanugage.noRecordFound = "<?php echo lang('no_record_found'); ?>";
    AppLanugage.print = "<?php echo lang('print'); ?>";
    AppLanugage.excel = "<?php echo lang('excel'); ?>";
    AppLanugage.printButtonTooltip = "<?php echo lang('print_button_help_text'); ?>";

    AppLanugage.fileUploadInstruction = "<?php echo lang('file_upload_instruction'); ?>";
    AppLanugage.fileNameTooLong = "<?php echo lang('file_name_too_long'); ?>";
    
    AppLanugage.custom = "<?php echo lang('custom'); ?>";
    AppLanugage.clear = "<?php echo lang('clear'); ?>";

</script>