<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa fa-file-text-o"></i>&nbsp; <?php echo lang("invoice_statistics"); ?>
    </div>
    <div class="panel-body ">
        <div id="invoice-payment-statistics-flotchart" style="width: 100%; height: 300px;"></div>
    </div>

</div>

<script type="text/javascript">
    $(document).ready(function () {
        var invoiceStatisticsFlotchart = function () {
            var invoices =<?php echo $invoices; ?>,
                payments = <?php echo $payments; ?>,
                dataset = [
                    {
                        data: invoices,
                        color: "rgba(220,220,220, 1)",
                        lines: {
                            show: true,
                            fill: 0.2
                        },
                        points: {
                            show: false
                        },
                        shadowSize: 0,
                    },
                    {
                        label: "<?php echo lang('invoice'); ?>",
                        data: invoices,
                        color: "rgba(220,220,220, 1)",
                        lines: {
                            show: false
                        },
                        points: {
                            show: true,
                            fill: true,
                            radius: 4,
                            fillColor: "#fff",
                            lineWidth: 1
                        },
                        curvedLines: {
                            apply: false
                        },
                        shadowSize: 0
                    },
                    {
                        data: payments,
                        color: "rgba(0, 179, 147, 1)",
                        lines: {
                            show: true,
                            fill: 0.2
                        },
                        points: {
                            show: false
                        },
                        shadowSize: 0,
                    },
                    {
                        label: "<?php echo lang('payment'); ?>",
                        data: payments,
                        color: "rgba(0, 179, 147, 1)",
                        lines: {
                            show: false
                        },
                        points: {
                            show: true,
                            fill: true,
                            radius: 4,
                            fillColor: "#fff",
                            lineWidth: 1
                        },
                        shadowSize: 0,
                        curvedLines: {
                            apply: false,
                        }
                    }
                ];

            $.plot($("#invoice-payment-statistics-flotchart"), dataset, {
                series: {
                    curvedLines: {
                        apply: true,
                        active: true,
                        monotonicFit: true
                    }
                },
                yaxis: {
                    min: 0
                },
                xaxis: {
                    ticks: [[1, "<?php echo lang('january'); ?> (1)"], [2, "<?php echo lang('february'); ?> (2)"], [3, "<?php echo lang('march'); ?> (3)"], [4, "<?php echo lang('april'); ?> (4)"], [5, "<?php echo lang('may'); ?> (5)"], [6, "<?php echo lang('june'); ?> (6)"], [7, "<?php echo lang('july'); ?> (7)"], [8, "<?php echo lang('august'); ?> (8)"], [9, "<?php echo lang('september'); ?> (9)"], [10, "<?php echo lang('october'); ?> (10)"], [11, "<?php echo lang('november'); ?> (11)"], [12, "<?php echo lang('december'); ?> (12)"]]
                },
                grid: {
                    color: "#bbb",
                    hoverable: true,
                    borderWidth: 0,
                    backgroundColor: '#FFF'
                },
                tooltip: {
                    show: true,
                    content: function (x, y, z) {
                        if (x) {
                            return "%s: " + toCurrency(z);
                        } else {
                            return false;
                        }
                    },
                    defaultTheme: false
                }
            });
        };

        invoiceStatisticsFlotchart();
    });
</script>

