<div class="panel" style="min-height: 500px">
    <?php $s=$this->session->userdata("gantt_options"); ?>
    <div class="tab-title clearfix">
        <h4 class="pull-right"><?php echo lang('gantt'); ?></h4>
        <div class="p10 mr10 pull-left">
            <?php if ($show_project_members_dropdown): ?>

                <div class="dropdown" style="float: right;">
                    <button class="btn btn-dafault dropdown-toggle" type="button" style="width: 150px"
                            data-toggle="dropdown"><span id="deadline_span">-<?php echo lang('deadline') ?>-</span> <span class="caret"></span></button>
                    <ul class="dropdown-menu" style="padding: 4px 7px 10px 5px;width: 200px;">
                        <li><a href="#" class="deadline-list" data-x="0">- <?php echo lang('deadline') ?> -</a></li>
                        <li><a href="#" class="deadline-list" data-x="1"><?php echo lang('expired') ?></a></li>
                        <li><a href="#" class="deadline-list" data-x="2"><?php echo lang('today') ?></a></li>
                        <li><a href="#" class="deadline-list" data-x="3"><?php echo lang('tomorrow') ?></a></li>
                        <li><a href="#" class="deadline-list"
                               data-x="4"><?php echo sprintf(lang('in_number_of_days'), 7); ?></a></li>
                        <li><a href="#" class="deadline-list"
                               data-x="5"><?php echo sprintf(lang('in_number_of_days'), 15); ?></a></li>
                        <li><a href="#" id="custom" data-x="6"><?php echo lang('custom') ?></a></li>
                    </ul>
                </div>

                <div class="dropdown" style="float: right;">
                    <button class="btn btn-dafault dropdown-toggle" type="button" data-toggle="dropdown">الفلاتر <span
                                class="caret"></span></button>
                    <div class="dropdown-menu" style="padding: 4px 15px 13px 5px;width: 338px;">
                        <div style="margin-top: 10px">
                            <div style="width: 100px; display: inline-block;">العرض حسب :</div>
                            <?php echo form_dropdown("gantt-group-by", array("all" => "الكل", "milestones" => lang("milestones"), "members" => lang("assign_to")), array(), "class='select2 w200 ' id='gantt-group-by'"); ?>
                        </div>

                        <div style="margin-top: 10px">
                            <div style="width: 100px; display: inline-block;">المراحل :</div>
                            <?php echo form_input(array("id" => "gantt-milestone-dropdown", "name" => "gantt-milestone-dropdown", "class" => "select2 w200 ", "placeholder" => lang('milestone'))); ?>
                            <div style="margin-top: 10px"></div>
                        </div>

                        <div style="margin-top: 10px">
                            <div style="width: 100px;  display: inline-block;">المكلف :</div>
                            <?php echo form_input(array("id" => "gantt-members-dropdown", "name" => "gantt-members-dropdown", "class" => "select2 w200", "placeholder" => lang('assign_to'))); ?>
                        </div>

                        <div style="margin-top: 10px">
                            <div style="width: 100px; display: inline-block;">المتعاونون :</div>
                            <select name="gantt-collaborators-dropdown[]" class="select2"
                                    id="gantt-collaborators-dropdown" multiple style="min-width: 200px">
                                <?php foreach (json_decode($project_members_dropdown) as $m): ?>
                                    <?php if ($m->id): ?>
                                        <option value="<?= $m->id ?>"><?= $m->text ?></option>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </select>
                        </div>

                        <div style="margin-top: 10px">
                            <div style="width: 100px;  display: inline-block;">الحالة :</div>
                            <?php echo form_input(array("id" => "gantt-status-dropdown", "name" => "gantt-status-dropdown", "class" => "select2 w200  ml10", "placeholder" => lang('status'))); ?>
                        </div>

                        <div style="margin-top: 10px">
                            <div style="width: 100px; display: inline-block">التاريخ من :</div>
                            <?php echo form_input(array("id" => "start_date", "name" => "start_date", "class" => "form-control ", "style" => "width:200px;display: inline-block;background:#F5F8F9;border: none;box-shadow: none;")); ?>
                        </div>

                        <div style="margin-top: 10px">
                            <span style="width: 100px; display: inline-block">التاريخ إلى :</span>
                            <?php echo form_input(array("id" => "end_date", "name" => "end_date", "class" => "form-control ", "style" => "width:200px;display: inline-block;background:#F5F8F9;border: none;box-shadow: none;")); ?>
                        </div>


                        <div style="margin-top: 10px;width: 100px;">
                            <button type="button" class="btn btn-primary btn-block reload-gantt" style="margin: 0px;">
                                فلترة
                            </button>
                        </div>
                    </div>
                </div>

            <?php endif; ?>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="w100p pt10">
        <div id="gantt-chart" style="width: 100%;"></div>
        <div id="scroll-to-me"></div>
    </div>

</div>

<script type="text/javascript">

    var loadGantt = function (group_by, milestone_id, member_id, colla, status, start_date, end_date, deadline) {
        group_by = group_by || "all";
        milestone_id = milestone_id || "0";
        member_id = member_id || "0";
        status = status || "0";
        start_date = start_date || "0";
        end_date = end_date || "0";
        colla = colla || "0";
        deadline = deadline || "0"
        var url = "<?php echo get_uri("projects/gantt_data/" . $project_id); ?>" + "/" + group_by + "/" + milestone_id + "/" + member_id + "/" + colla + "/" + status + "/" + start_date + "/" + end_date + "/" + deadline;

        $("#gantt-chart").html("<div style='height:100px;'></div>");
        appLoader.show({container: "#gantt-chart", css: "right:50%;"});
        $("#gantt-chart").ganttView({
            dataUrl: url,
            monthNames: AppLanugage.months,
            dayText: "<?php echo lang('day'); ?>",
            daysText: "<?php echo lang('days'); ?>"
        });
    };

    function ascrollto(id) {
        var etop = $('#' + id).offset().top;
        $('html, body').animate({
            scrollTop: etop
        }, 1000);
    }

    var sendData;

    $(document).ready(function () {
        var $ganttGroupBy = $("#gantt-group-by"),
            $ganttMilestone = $("#gantt-milestone-dropdown"),
            $ganttMembers = $("#gantt-members-dropdown"),
            $ganttStatus = $("#gantt-status-dropdown"),
            $ganttcollaboratorsdropdown = $("#gantt-collaborators-dropdown"),
            $start_date = $("#start_date"),
            $end_date = $("#end_date");
        var $deadline = "";

        setDatePicker("#start_date, #end_date");
        $ganttGroupBy.select2();
        $ganttMilestone.select2({data: <?php echo $milestone_dropdown; ?>});
        if ($ganttMembers.length) {
            $ganttMembers.select2({data: <?php echo $project_members_dropdown; ?>});
        }
        $ganttcollaboratorsdropdown.select2({placeholder: "المتعاونون"});
        $ganttStatus.select2({data: <?php echo $status_dropdown; ?>});

        $(".reload-gantt").click(function () {
            sendData();
        });

        $('#custom').datepicker({
            autoclose: true,
            language: "custom",
            todayHighlight: true,
            weekStart: AppHelper.settings.firstDayOfWeek,
            format: "yyyy-mm-dd"
        })
            .on("changeDate", function (e) {
                $("#deadline_span").html("<?php echo lang('custom') ?>");
                $deadline = e.format(0, "yyyy-mm-dd");
                sendData();
            });

        $(".deadline-list").on("click", function (e) {
            e.preventDefault();
            var x = $(this).data('x');
            var txt = $(this).html();
            $("#deadline_span").html(txt);
            switch (x) {
                case 0:
                    $deadline = "";
                    break;
                case 1:
                    $deadline = "expired";
                    break;
                case 2:
                    $deadline = moment().format("YYYY-MM-DD");
                    break;
                case 3:
                    $deadline = moment().add(1, 'days').format("YYYY-MM-DD");
                    break;
                case 4:
                    $deadline = moment().add(7, 'days').format("YYYY-MM-DD");
                    break;
                case 5:
                    $deadline = moment().add(15, 'days').format("YYYY-MM-DD");
                    break;
            }
            sendData();
        });

        sendData = function(session=null) {
            var group_by = $ganttGroupBy.val() || "all",
                milestone_id = 0, member_id = 0,
                status = $ganttStatus.val();
            var milestone_id = $ganttMilestone.val();
            var member_id = $("#gantt-members-dropdown").val();
            var start_date = $start_date.val();
            var end_date = $end_date.val();
            var colla = encodeURIComponent($ganttcollaboratorsdropdown.val()) || 0;
            var deadline = $deadline || "0";
            if(session){
                <?php if(isset($s['group_by']) and $s['group_by']!='all'):?>
                group_by= "<?=$s['group_by'];?>";
                $ganttGroupBy.select2("val", group_by);
                <?php endif;?>
                <?php if(isset($s['assigned_to']) and $s['assigned_to']!=0):?>
                member_id= "<?=$s['assigned_to'];?>";
                $ganttMembers.select2("val", member_id);
                <?php endif;?>
                <?php if(isset($s['milestone_id']) and $s['milestone_id']!=''):?>
                milestone_id= "<?=$s['milestone_id'];?>";
                $ganttMilestone.select2("val", milestone_id);
                <?php endif;?>
                <?php if(isset($s['colla']) and $s['colla']!=''):?>
                colla= "<?=$s['colla'];?>";
                $ganttcollaboratorsdropdown.select2("val", colla.split(','));
                colla=encodeURIComponent(colla);
                <?php endif;?>
                <?php if(isset($s['status_id']) and $s['status_id']!=''):?>
                status= "<?=$s['status_id'];?>";
                $ganttStatus.select2("val", status);
                <?php endif;?>
                <?php if(isset($s['start_datex']) and $s['start_datex']!=''):?>
                start_date= "<?=$s['start_datex'];?>";
                $start_date.val(start_date);
                <?php endif;?>
                <?php if(isset($s['end_datex']) and $s['end_datex']!=''):?>
                end_date= "<?=$s['end_datex'];?>";
                $end_date.val(end_date);
                <?php endif;?>
                <?php if(isset($s['deadlinex']) and $s['deadlinex']!='0'):?>
                var x= "<?=$s['deadlinex'];?>";
                $deadline = deadline = x;
                if(x=="expired"){
                    $("#deadline_span").html("<?php echo lang('expired') ?>");
                }else if(x==moment().format("YYYY-MM-DD")){
                    $("#deadline_span").html("<?php echo lang('today') ?>");
                }else if(x==moment().add(1, 'days').format("YYYY-MM-DD")){
                    $("#deadline_span").html("<?php echo lang('tomorrow') ?>");
                }else if(x==moment().add(7, 'days').format("YYYY-MM-DD")){
                    $("#deadline_span").html("<?php echo sprintf(lang('in_number_of_days'), 7); ?>");
                }else if(x==moment().add(15, 'days').format("YYYY-MM-DD")){
                    $("#deadline_span").html("<?php echo sprintf(lang('in_number_of_days'), 15); ?>");
                }else{
                    $("#deadline_span").html("<?php echo lang('custom') ?>");
                }
                <?php endif;?>
            }
            loadGantt(group_by, milestone_id, member_id, colla, status, start_date, end_date, deadline);
        }

        sendData(1);

    });

    function ooz() {
        sendData();
    }
</script>

<style>
    ul.select2-choices {
        padding-right: 6px !important;
        padding-left: 0px !important;
    }

    ul.select2-choices:after {
        content: "";
        position: absolute;
        right: 6px;
        top: 50%;
        transform: translateY(-50%);
        border-top: 5px solid #909090;
        border-left: 3px solid transparent;
        border-right: 3px solid transparent;
    }
</style>