<!DOCTYPE html>
<html lang="ar">
    <?php $this->load->view('includes/head'); ?>
    <body >
        <?php
        if ($topbar) {
            $this->load->view($topbar);
        }
        ?>
        <div id="content" class="box">
            <?php
            if ($left_menu) {
                $this->load->view($left_menu);
            }
            ?>
            <div id="page-container" class="box-content">
                <div id="pre-loader">
                    <div id="pre-loade" class="app-loader"><div class="loading"></div></div>
                </div>
                <div class="scrollable-page">
                    <?php
                    if (isset($content_view) && $content_view != "") {
                        $this->load->view($content_view);
                    }
                    ?>
                </div>
            </div>
        </div>
        <?php $this->load->view('modal/index'); ?>
        <?php $this->load->view('modal/confirmation'); ?>
        <div style='display: none;'>
            <script type='text/javascript'>
<?php
$error_message = $this->session->flashdata("error_message");
$success_message = $this->session->flashdata("success_message");
if (isset($error)) {
    echo 'appAlert.error("' . $error . '");';
}
if (isset($error_message)) {
    echo 'appAlert.error("' . $error_message . '");';
}
if (isset($success_message)) {
    echo 'appAlert.success("' . $success_message . '", {duration: 10000});';
}
?>
            </script>
        </div>

    <style>
        .select2-search-choice{
            background: #00adff !important;
            border-color: #00adff !important;
            color: #fff !important;
            padding: 3px 18px 3px 5px !important;
            float: right !important;
        }
        table.dataTable thead .sorting_asc:after,table.dataTable thead .sorting_desc:after,table.dataTable thead .sorting:after{
            display: none;
        }
        table.dataTable thead .sorting, table.dataTable thead .sorting_asc, table.dataTable thead .sorting_desc, table.dataTable thead .sorting_asc_disabled, table.dataTable thead .sorting_desc_disabled{
            background-position: center left;
        }
        .select2-results .select2-result-label{
            direction: ltr;
        }
    </style>
    </body>
</html>